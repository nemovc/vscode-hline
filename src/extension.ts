// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

/* ------------------------------------ [ this is a test  ] ------------------------------------- */
const MIN_DASHES = 3;
const DEFAULT_WIDTH = 100;
const PREPEND = '/* ';
const MID_PREPEND = ' [ ';
const MID_APPEND = ' ] ';
const APPEND = ' */';
const DASH_CHAR = '-';

const AUTO_LENGTH = `${PREPEND}${MID_PREPEND}${MID_APPEND}${APPEND}`.length;

const HLINE_REGEX = new RegExp(/^([ \t]*)\/\* -+ \[ (.*) ] -+ \*\/$/);

export function activate(context: vscode.ExtensionContext) {

  let disposable = vscode.commands.registerCommand('hline.format', () => {

    // retrieve thing we require to calculate
    const endCol = (vscode.workspace.getConfiguration().get('editor.rulers') as [number])[0] || DEFAULT_WIDTH;
    const tabSize = vscode.workspace.getConfiguration().get('editor.tabSize') as number;
    const documentText = vscode.window.activeTextEditor?.document.getText().split('\n');

    vscode.window.activeTextEditor?.edit((editBuilder) => {
      documentText?.forEach((line, lineNumber) => {
        const match = line.match(HLINE_REGEX);
        if (match !== null) {
          const [, tabs, inner] = match;
          // calculate number of dashes to use
          const startLength = tabs.replace(/\t/g, ' '.repeat(tabSize)).length;
          const contentLength = inner.length;
          const remainingLength = endCol - (startLength + AUTO_LENGTH + contentLength);
          const preDashes = Math.max(Math.floor(remainingLength / 2), MIN_DASHES);
          const appDashes = Math.max(Math.ceil(remainingLength / 2), MIN_DASHES);
          // build new string
          const newStr = `${tabs}${PREPEND}${DASH_CHAR.repeat(preDashes)}${MID_PREPEND}${inner}${MID_APPEND}${DASH_CHAR.repeat(appDashes)}${APPEND}`;
          editBuilder.replace(new vscode.Range(new vscode.Position(lineNumber, 0), new vscode.Position(lineNumber, line.length)), newStr);
        }
      });

    });
  });

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() { }
